=== Custom Post Types UI Extended ===
Contributors:      WebDevStudios
Tags: custom post types
Requires at least: 3.9
Tested up to:      4.5.2
Stable tag:        1.2.1
License:           GPLv2
License URI:       http://www.gnu.org/licenses/gpl-2.0.html


== Description ==


Custom Post Type UI Extended provides add-on functionality to the widely popular Custom Post Type UI plugin.

Current features include a shortcode builder to make it simple to display data from your custom post types in a page or post, multisite support to allow for easy creation of network wide post types and taxonomies.


== Installation ==


= Manual Installation =

1. Upload the CPTUI-Extended folder to the plugins directory in your WordPress installation
2. Activate the plugin.
3. Navigate to the "CPTUI Extended" Menu.

Upload Zip File

1. Navigate to Plugins -> Add New.
2. Click the "Upload Plugin" button.
3. Select the "cptui-extended.zip" file from your computer.
4. Click "Install Now".
5. Click the "Activate Plugin" link.
6. Navigate to the "CPTUI Extended" Menu.


== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

See CHANGELOG.md


== Upgrade Notice ==
