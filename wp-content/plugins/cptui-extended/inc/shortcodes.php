<?php
/**
 * Default Shortcodes for CPTUI Extended
 *
 * @package CPTUIExtended
 * @author  WebDevStudios
 * @license GPLV2
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Register Shortcodes
 *
 * @internal
 *
 * @return void
 */
function cptui_register_core_shortcodes() {

	if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	}

	cptui_register_shortcode( 'cptui_default_shortcode' );
	cptui_register_shortcode( 'cptui_single_page_shortcode' );
	cptui_register_shortcode( 'cptui_single_post_shortcode' );
	cptui_register_shortcode( 'cptui_list_post_shortcode' );
	cptui_register_shortcode( 'cptui_taxononmy_list_shortcode' );
	cptui_register_shortcode( 'cptui_slider_shortcode' );

	/**
	 * Check if WooCommerce is active
	 */
	if ( is_plugin_active( 'woocommerce/woocommerce.php' )
		|| is_plugin_active_for_network( 'woocommerce/woocommerce.php' ) ) {
		cptui_register_shortcode( 'cptui_woo_product_shortcode' );
	}

	/**
	 * Check if Easy Digital Downloads is active
	 */
	if ( is_plugin_active( 'easy-digital-downloads/easy-digital-downloads.php' )
		|| is_plugin_active_for_network( 'easy-digital-downloads/easy-digital-downloads.php' ) ) {
		cptui_register_shortcode( 'cptui_edd_download_shortcode' );
	}

}
add_action( 'cptuiext_loaded', 'cptui_register_core_shortcodes', 5 );

/**
 * Default shortcode.
 *
 * @internal
 *
 * @return array Shortcode data including CMB2 fields
 */
function cptui_default_shortcode() {

	$shortcode = array(
		'id' => 'default_shortcode',
		'name' => __( 'Default', 'cptuiext' ),
		'template' => 'posts',
		'template_location' => cptui_template_path(),
		'style'             => 'cptui_default_css',
		'post_type'         => 'all',
		'description'       => ' ',
		'fields'            => array(
			array(
				'name' => __( 'Title', 'cptuiext' ),
				'description' => __( 'Title to display above shortcode', 'cptuiext' ),
				'id'   => 'title',
				'type' => 'text',
			),
			array(
				'name' => __( 'Featured Image', 'cptuiext' ),
				'description' => __( 'Would you like a featured image', 'cptuiext' ),
				'id'   => 'featured_image',
				'type' => 'checkbox',
			),
			array(
				'name'		=> __( 'Choose Posts', 'cptuiext' ),
				'description' => __( 'Enter comma seperated post id(s) or click search icon. Default is all posts.', 'cptuiext' ),
				'id'		  => 'attached_post',
				'type'		=> 'post_search_text',
				'post_type'   => 'post',
				'select_type' => 'checkbox',
				'select_behavior' => 'add',
			),
			array(
				'name'	=> __( 'Order By', 'cptuiext' ),
				'id'	  => 'order_by',
				'type'	=> 'select',
				'show_option_none' => true,
				'default' => 'none',
				'options' => array(
					'date' => 'Post Date',
					'id' => 'Post ID',
					'title' => 'Title',
				),
			),
			array(
				'name'	=> __( 'Order', 'cptuiext' ),
				'id'	  => 'order',
				'type'	=> 'radio',
				'default' => 'DESC',
				'options' => array(
					'ASC' => 'Ascending',
					'DESC' => 'Descending',
				),
			),
			array(
				'name'	=> __( 'Taxonomy', 'cptuiext' ),
				'id'	  => 'taxonomy',
				'type'	=> 'multicheck',
				'options' => array(
					'none' => 'None',
				),
				'before' => 'cptui_taxonomy_set_field_data_attr',
			),
		),
	);

	return apply_filters( 'cptui_default_shortcode', $shortcode );

}

/**
 * Single page shortcode.
 *
 * @internal
 *
 * @return array Shortcode data including CMB2 fields
 */
function cptui_single_page_shortcode() {

	$shortcode = array(
		'id' => 'single_page_shortcode',
		'name' => __( 'Single Page', 'cptuiext' ),
		'template' => 'page',
		'template_location' => cptui_template_path(),
		'style'             => 'cptui_default_css',
		'post_type'         => 'page',
		'description'       => ' ',
		'fields'            => array(
			array(
				'name' => __( 'Title', 'cptuiext' ),
				'description' => __( 'Title to display above shortcode.', 'cptuiext' ),
				'id'   => 'title',
				'type' => 'text',
			),
			array(
				'name' => __( 'Page', 'cptuiext' ),
				'description' => __( 'select page.', 'cptuiext' ),
				'id'   => 'page_id',
				'type' => 'select',
				'show_option_none' => true,
				'default'		  => 'none',
				'options' => cptui_get_pages(),
			),
			array(
				'name' => __( 'Featured Image', 'cptuiext' ),
				'description' => __( 'Would you like a featured image', 'cptuiext' ),
				'id'   => 'featured_image',
				'type' => 'checkbox',
			),
		),
	);

	return apply_filters( 'single_page_shortcode', $shortcode );

}

/**
 * Single post shortcode.
 *
 * @internal
 *
 * @return array Shortcode data including CMB2 fields.
 */
function cptui_single_post_shortcode() {

	$shortcode = array(
		'id' => 'single_post_shortcode',
		'name' => __( 'Single Post', 'cptuiext' ),
		'template' => 'single',
		'template_location' => cptui_template_path(),
		'style'             => 'cptui_default_css',
		'post_type'         => 'post',
		'description'       => ' ',
		'fields'            => array(
			array(
				'name' => __( 'Title', 'cptuiext' ),
				'description' => __( 'Title to display above post', 'cptuiext' ),
				'id'   => 'title',
				'type' => 'text',
			),
			array(
				'name'		=> __( 'Choose Post', 'cptuiext' ),
				'description' => __( 'Enter comma seperated post id(s) or click search icon. Default is all posts.', 'cptuiext' ),
				'id'		  => 'post_id',
				'type'		=> 'post_search_text',
				'post_type'   => 'post',
				'select_type' => 'radio',
				'select_behavior' => 'add',
			),
			array(
				'name' => __( 'Featured Image', 'cptuiext' ),
				'description' => __( 'Would you like a featured image', 'cptuiext' ),
				'id'   => 'featured_image',
				'type' => 'checkbox',
			),
		),
	);

	return apply_filters( 'single_post_shortcode', $shortcode );

}

/**
 * Cptui_list_post_shortcode shortcode
 *
 * @return array Shortcode data including CMB2 fields
 */
function cptui_list_post_shortcode() {

	$shortcode = array(
		'id' => 'list_shortcode',
		'name' => __( 'List', 'cptuiext' ),
		'template' => 'list',
		'template_location' => cptui_template_path(),
		'style' => 'cptui_default_css',
		'post_type' => 'all',
		'description' => ' ',
		'fields' => array(
			array(
				'name' => __( 'Excerpt', 'cptuiext' ),
				'description' => __( 'Would you like to show the post excerpt?', 'cptuiext' ),
				'id'   => 'excerpt',
				'type' => 'checkbox',
			),
			array(
				'name'	=> __( 'List Type', 'cptuiext' ),
				'id'	  => 'list_type',
				'type'	=> 'radio',
				'options' => array(
					'ul' => __( 'Unordered - Display bullet points next to each item', 'cptuiext' ),
					'ol' => __( 'Ordered - Display numbers 1, 2, 3, etc next to each item', 'cptuiext' ),
				),
			),
			array(
				'name'		=> __( 'Choose Posts', 'cptuiext' ),
				'description' => __( 'Enter comma seperated post id(s) or click search icon. Default is all posts.', 'cptuiext' ),
				'id'		  => 'attached_post',
				'type'		=> 'post_search_text',
				'post_type'   => 'post',
				'select_type' => 'checkbox',
				'select_behavior' => 'add',
			),
			array(
				'name'	=> __( 'Order By', 'cptuiext' ),
				'id'	  => 'order_by',
				'type'	=> 'select',
				'show_option_none' => true,
				'default' => 'none',
				'options' => array(
					'date' => 'Post Date',
					'id' => 'Post ID',
					'title' => 'Title',
				),
			),
			array(
				'name'	=> __( 'Order', 'cptuiext' ),
				'id'	  => 'order',
				'type'	=> 'radio',
				'default' => 'DESC',
				'options' => array(
					'ASC' => 'Ascending',
					'DESC' => 'Descending',
				),
			),
			array(
				'name' => __( 'Amount', 'cptuiext' ),
				'description' => __( 'Enter a number for the amount of posts to list. "Choose Posts" field overides this option.', 'cptuiext' ),
				'id'   => 'amount',
				'type' => 'text',
			),
		),
	);

	return apply_filters( 'list_post_shortcode', $shortcode );

}

/**
 * Woo_product_shortcode shortcode
 *
 * @return array Shortcode data including CMB2 fields
 */
function cptui_woo_product_shortcode() {

	$shortcode = array(
		'id' => 'woo_product_shortcode',
		'name' => __( 'Woo Product', 'cptuiext' ),
		'template' => 'woo',
		'template_location' => cptui_template_path(),
		'style' => 'cptui_default_css',
		'post_type' => 'product',
		'description' => ' ',
		'fields' => array(
			array(
				'name' => __( 'Title', 'cptuiext' ),
				'description' => __( 'Title to display above shortcode', 'cptuiext' ),
				'id'   => 'title',
				'type' => 'text',
			),
			array(
				'name' => __( 'Featured Image', 'cptuiext' ),
				'description' => __( 'Would you like a featured image', 'cptuiext' ),
				'id'   => 'featured_image',
				'type' => 'checkbox',
			),
			array(
				'name'		=> __( 'Choose Product', 'cptuiext' ),
				'description' => __( 'Enter comma seperated product id(s) or click search icon. Default is all products.', 'cptuiext' ),
				'id'		  => 'attached_product',
				'type'		=> 'post_search_text',
				'post_type'   => 'product',
				'select_type' => 'checkbox',
				'select_behavior' => 'add',
			),
			array(
				'name'	=> __( 'Order By', 'cptuiext' ),
				'id'	  => 'order_by',
				'type'	=> 'select',
				'show_option_none' => true,
				'default' => 'none',
				'options' => array(
					'date' => 'Post Date',
					'id' => 'Post ID',
					'title' => 'Title',
				),
			),
			array(
				'name'	=> __( 'Order', 'cptuiext' ),
				'id'	  => 'order',
				'type'	=> 'radio',
				'default' => 'DESC',
				'options' => array(
					'ASC' => 'Ascending',
					'DESC' => 'Descending',
				),
			),
		),
	);

	return apply_filters( 'woo_product_shortcode', $shortcode );
}

/**
 * Edd_download_shortcode shortcode
 *
 * @return array Shortcode data including CMB2 fields
 */
function cptui_edd_download_shortcode() {

	$shortcode = array(
		'id' => 'edd_download_shortcode',
		'name' => __( 'EDD Download', 'cptuiext' ),
		'template' => 'edd',
		'template_location' => cptui_template_path(),
		'style' => 'cptui_default_css',
		'post_type' => 'download',
		'description' => ' ',
		'fields' => array(
			array(
				'name' => __( 'Title', 'cptuiext' ),
				'description' => __( 'Title to display above shortcode', 'cptuiext' ),
				'id'   => 'title',
				'type' => 'text',
			),
			array(
				'name' => __( 'Featured Image', 'cptuiext' ),
				'description' => __( 'Would you like a featured image', 'cptuiext' ),
				'id'   => 'featured_image',
				'type' => 'checkbox',
			),
			array(
				'name'		=> __( 'Choose Download', 'cptuiext' ),
				'description' => __( 'Enter comma seperated download id(s) or click search icon. Default is all downlaods.', 'cptuiext' ),
				'id'		  => 'attached_download',
				'type'		=> 'post_search_text',
				'post_type'   => 'download',
				'select_type' => 'checkbox',
				'select_behavior' => 'add',
			),
			array(
				'name'	=> __( 'Order By', 'cptuiext' ),
				'id'	  => 'order_by',
				'type'	=> 'select',
				'show_option_none' => true,
				'default' => 'none',
				'options' => array(
					'date' => 'Post Date',
					'id' => 'Post ID',
					'title' => 'Title',
				),
			),
			array(
				'name'	=> __( 'Order', 'cptuiext' ),
				'id'	  => 'order',
				'type'	=> 'radio',
				'default' => 'DESC',
				'options' => array(
					'ASC' => 'Ascending',
					'DESC' => 'Descending',
				),
			),
		),
	);

	return apply_filters( 'edd_download_shortcode', $shortcode );
}

/**
 * cptui_taxononmy_list_shortcode shortcode
 *
 * @return array Shortcode data including CMB2 fields
 */
function cptui_taxononmy_list_shortcode() {

	$shortcode = array(
		'id' => 'taxonomy_list_shortcode',
		'name' => __( 'Taxonomy List', 'cptuiext' ),
		'template' => 'taxonomy-list',
		'template_location' => cptui_template_path(),
		'style' => 'cptui_default_css',
		'post_type' => 'all',
		'description' => ' ',
		'fields' => array(
			array(
				'name'	=> __( 'List Type', 'cptuiext' ),
				'id'	  => 'list_type',
				'type'	=> 'radio',
				'options' => array(
					'ul' => __( 'Unordered - Display bullet points next to each item', 'cptuiext' ),
					'ol' => __( 'Ordered - Display numbers 1, 2, 3, etc next to each item', 'cptuiext' ),
				),
			),
			array(
				'name'	=> __( 'Taxonomy', 'cptuiext' ),
				'id'	  => 'taxonomy',
				'type'	=> 'multicheck',
				'options' => array(
					'none' => 'None',
				),
				'before' => 'cptui_taxonomy_set_field_data_attr',
			),
		),
	);

	return apply_filters( 'cptui_taxononmy_list_shortcode', $shortcode );

}

/**
 * Cptui_slider_shortcode shortcode
 *
 * @return array Shortcode data including CMB2 fields
 */
function cptui_slider_shortcode() {

	$shortcode = array(
		'id' => 'slider_shortcode',
		'name' => __( 'Post Slider', 'cptuiext' ),
		'template' => 'slider',
		'template_location' => cptui_template_path(),
		'style' => array( 'cptui_default_css', 'cptui_slick_slider_css', 'cptui_slick_slider_theme' ),
		'script' => array( 'cptui_slick_slider_js', 'cptui_slider_js' ),
		'post_type' => 'all',
		'description' => ' ',
		'fields' => array(
			array(
				'name'		=> __( 'Choose Posts', 'cptuiext' ),
				'description' => __( 'Enter comma seperated post id(s) or click search icon. Default is all posts.', 'cptuiext' ),
				'id'		  => 'attached_post',
				'type'		=> 'post_search_text',
				'post_type'   => 'post',
				'select_type' => 'checkbox',
				'select_behavior' => 'add',
			),
			array(
				'name' => __( 'Slider Height', 'cptuiext' ),
				'description' => __( 'Enter pixel height for slider. Example: 300. Default is 300 pixels tall.', 'cptuiext' ),
				'id'   => 'height',
				'type' => 'text',
			),
			array(
				'name' => __( 'Post Title', 'cptuiext' ),
				'description' => __( 'Show post title below slider image.', 'cptuiext' ),
				'id'   => 'show_title',
				'type' => 'checkbox',
			),
			array(
				'name' => __( 'Slider Bullets', 'cptuiext' ),
				'description' => __( 'Show bullets below slider.', 'cptuiext' ),
				'id'   => 'show_bullets',
				'type' => 'checkbox',
			),
			array(
				'name' => __( 'Auto Play', 'cptuiext' ),
				'description' => __( 'Auto Play the slides.', 'cptuiext' ),
				'id'   => 'autoplay',
				'type' => 'checkbox',
			),
		),
	);

	return apply_filters( 'cptui_slider_shortcode', $shortcode );

}

/**
 * Cptui_taxonomy_set_field_data_attr
 *
 * @param array $args  cmb2 field arguments.
 * @param array $field cmb2 field data.
 */
function cptui_taxonomy_set_field_data_attr( $args, $field ) {
	$field->args['attributes']['data-field'] = $args['id'];
}

/**
 * Register CPTUI default scripts.
 *
 * @internal
 */
function cptui_register_shortcode_scripts() {
	wp_register_style( 'cptui_default_css', cptui_extended()->url() . 'assets/css/style.css' );
	wp_register_style( 'cptui_slick_slider_css', cptui_extended()->url() . 'assets/js/slick/slick.css' );
	wp_register_style( 'cptui_slick_slider_theme', cptui_extended()->url() . 'assets/js/slick/slick-theme.css' );
	wp_register_script( 'cptui_slick_slider_js', cptui_extended()->url() . 'assets/js/slick/slick.min.js' );
	wp_register_script( 'cptui_slider_js', cptui_extended()->url() . 'assets/js/slider.js' );
}
add_action( 'wp_enqueue_scripts', 'cptui_register_shortcode_scripts' );
