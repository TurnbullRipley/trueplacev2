<?php
/**
 * CPTUI Extended Network
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @since 1.0.0
 */

/**
 * Redirects activation to welcom page.
 *
 * @internal
 *
 * @return void
 */
function cptui_do_activation_redirect() {

	if ( ! get_transient( '_cptui_activation_redirect' ) ) {
		return;
	}

	delete_transient( '_cptui_activation_redirect' );

	// Bail if activating from network, or bulk.
	if ( isset( $_GET['activate-multi'] ) ) {
		return;
	}

	$query_args = array( 'page' => 'cptui_about' );

	// Redirect to CPTUI Extended about page.
	wp_safe_redirect( add_query_arg( $query_args, cptui_get_admin_url( 'index.php' ) ) );
}
add_action( 'admin_init', 'cptui_do_activation_redirect', 1 );

/**
 * Get netowrk or single admin url
 *
 * @param  string $path   current path.
 * @return string
 */
function cptui_get_admin_url( $path = '' ) {

	// Links belong in network admin.
	if ( cptui_is_network_activated() ) {
		$url = network_admin_url( $path );
	} else {
		$url = admin_url( $path );
	}

	return $url;
}

/**
 * Boolean if plugin network activated.
 *
 * @return boolean is plugin network activated
 */
function cptui_is_network_activated() {

	// Default to is_multisite().
	$retval  = is_multisite();

	// Check the sitewide plugins array.
	$base    = cptui_extended()->basename;
	$plugins = get_site_option( 'active_sitewide_plugins' );

	// Override is_multisite() if not network activated.
	if ( ! is_array( $plugins ) || ! isset( $plugins[ $base ] ) ) {
		$retval = false;
	}

	/**
	 * Filters whether or not we're active at the network level.
	 *
	 * @since 1.0.0
	 *
	 * @param bool $retval Whether or not we're network activated.
	 */
	return (bool) apply_filters( 'cptui_is_network_activated', $retval );
}
