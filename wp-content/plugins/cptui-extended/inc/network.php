<?php
/**
 * CPTUI Extended Network
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @since 1.0.0
 */

/**
 * Add Extended options to network admin CPTUI screen.
 *
 * @internal
 */
function cptui_network_add_extended_options() {

	$tab = ( ! empty( $_GET ) && ! empty( $_GET['action'] ) && 'edit' === $_GET['action'] ) ? 'edit' : 'new';

	if ( 'edit' === $tab ) {
		$post_types = cptui_get_post_type_data();
		$selected_post_type = cptui_get_current_post_type();

		if ( $selected_post_type ) {
			if ( array_key_exists( $selected_post_type, $post_types ) ) {
				$current = $post_types[ $selected_post_type ];
			}
		}
	}

	$ui = new cptui_admin_ui();

	echo $ui->get_fieldset_start( array( 'id' => 'extended_expand', 'classes' => array( '' ) ) );
	echo $ui->get_legend_start();
	esc_attr_e( 'Extended', 'cptuiext' );
	echo $ui->get_legend_end();

	echo '<table class="form-table cptui-table">';

	if ( is_multisite() && is_network_admin() ) {
		echo '<input type="hidden" id="network_wide" name="cpt_custom_post_type[network_wide]" value="1" aria-required="false">';
	}

	echo $ui->get_tr_start() . $ui->get_th_start() . __( 'Add to Default Post Query', 'cptuiext' ) . $ui->get_th_end() . $ui->get_td_start();

	echo $ui->get_check_input( array(
		'checkvalue' => '1',
		'checked'    => ( isset( $current['cpt_to_post_query'] ) ) ? 'true' : 'false',
		'name'       => 'cpt_to_post_query',
		'namearray'  => 'cpt_to_post_query',
		'textvalue'  => '1',
		'labeltext'  => esc_html__( 'Add the post entries from this CPT to the default post query (i.e. your blog)' , 'cptuiext' ),
		'helptext'   => esc_attr__( 'Add the post entries from this CPT to the default post query (i.e. your blog)', 'cptuiext' ),
		'default'    => true,
		'wrap'       => false,
	) );

	echo $ui->get_td_end() . $ui->get_tr_end();

	echo '</table>';
	echo $ui->get_fieldset_end();

}
add_action( 'cptui_post_type_after_fieldsets', 'cptui_network_add_extended_options' );


/**
 * Filters CPTUI page title on network admin page.
 *
 * @internal
 *
 * @param  array $tabs Unmodified page tabs array.
 * @return array Modified page tabs array.
 */
function cptui_network_page_title_filter( $tabs = array() ) {
	if ( is_multisite() && is_network_admin() ) {
		$tabs['page_title'] = __( 'Manage Network Wide Post Types', 'cptuiext' );

		if ( isset( $_GET['page'] ) && 'cptui_manage_taxonomies' === $_GET['page'] ) {
			$tabs['page_title'] = __( 'Manage Network Wide Taxonomies', 'cptuiext' );
		}
	}
	return $tabs;
}
add_filter( 'cptui_get_tabs', 'cptui_network_page_title_filter', 999 );

/**
 * Adds selected CPTs to post query.
 *
 * @internal
 *
 * @param object $query WP_Query object.
 * @return object $query
 */
function cptui_add_cpt_to_query( $query ) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_home() ) {

		$cpts = get_option( 'cptui_post_types' );

		if ( ! empty( $cpts ) ) {
			$types = array( 'post' );
			foreach ( $cpts as $cpt ) {
				if ( isset( $cpt['cpt_to_post_query'] ) ) {
					$types[] = $cpt['name'];
				}
			}
			$types = apply_filters( 'cptui_add_cpt_to_query', $types );
			$query->set( 'post_type', $types );
		}
	}
	return $query;
}
add_action( 'pre_get_posts', 'cptui_add_cpt_to_query' );

/**
 * Flush rewrite after network tax/cpt edits.
 *
 * @internal
 */
function cptui_flush_rewrite() {

	if ( false !== ( $network_flush_ts = get_site_transient( 'cptui_rewrite_refresh' ) ) ) {

		$flush_ts = get_option( 'cptui_rewrite_refresh' );

		if ( $flush_ts !== $network_flush_ts ) {
			update_option( 'cptui_rewrite_refresh', $network_flush_ts );

			flush_rewrite_rules();
		}
	}
}
add_action( 'cptui_post_register_post_types', 'cptui_flush_rewrite', 999 );
