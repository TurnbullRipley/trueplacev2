# Changelog
All notable changes to this project will be documented in this file.

## 1.2.1 - 2016-06-21

### Bug Fixes
* Fix error with activation key option
* Fix to remove ads from CPTUI when Extended activated


## 1.2.0 - 2016-06-06

### Bug Fixes
* Fix for tax query.
* Fix for post type in query.
* Filterable relation for AND in tax query.

### Enhancements
* Taxonomy list shortcode.
* Post slider shortcode.
* Allow array of styles and scripts per shortcode.



## 1.1.1 - 2016-05-19

### Bug Fixes
* Fix for taxonomy label selecting first input
* Fix for taxonomy not filtering query
* Use AND for tax query
* Use tax slug instead of Name



## 1.1.0 - 2016-04-19

### Bug Fixes
* Better description for search on modal.
* Fixes EDD downloads not displaying if none chosen.
* Fixed Woo Product template not displaying title.
* Fix for quotes in shortcode attributes.
* Fixed EDD Default template displaying posts, not downloads.
* Fix for custom CPT, search box shows blog posts instead of CPT posts.


### Enhancements
* Only show featured image column if CPT supports it in post list columns.
* Add dismiss button to License Key message.
* Add title and featured image options to the WooCommerce Product modal.
* Add quantity option to list template.
* Make the list templates available to all CPTs.
* New templates for WooCommerce, Easy Digital Downloads and ordered/unordered list.
* Enhanced HTML in templates. Enhanced CSS.
* 66 template hooks added for displaying custom data



## 1.0.0 - 2016-04-05

Initial Release.
