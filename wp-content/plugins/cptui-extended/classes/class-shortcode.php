<?php
/**
 * CPTUIEXT_Shortcode class
 *
 * @package CPTUIExtended
 * @subpackage Loader
 * @author WebDevStudios
 * @since 1.0.0
 */

if ( ! class_exists( 'CPTUIEXT_Shortcode', false ) ) {

	class CPTUIEXT_Shortcode extends WDS_Shortcodes {

		/**
		 * The Shortcode Tag
		 *
		 * @var string
		 */
		public $shortcode = 'cptui';

		/**
		 * Default attributes applied tot he shortcode.
		 *
		 * @var array
		 */
		public $atts_defaults = array(
			'some_default_key' => 'default value',
		);

		/**
		 * Shortcode Output
		 */
		public function shortcode() {

			// Parse default shortcode attributes.
			$attributes = $this->shortcode_object->atts;
			$default_atts = cptui_get_default_atts( $attributes['cptui_shortcode'] );
			$attributes = apply_filters( 'cptui_shortcode_attributes', wp_parse_args( $attributes, $default_atts ) );

			foreach ( $attributes as $attribute => $value ) {
				$result = substr( $attribute, 0, 8 );
				if ( 'taxonomy' === $result ) {
					$string = explode( 'taxonomy-', $attribute );
					if ( isset( $string[1] ) ) {
						$attributes['taxonomy'][ $string[1] ] = explode( ',', $attributes[ $attribute ] );
					}
					unset( $attributes[ $attribute ] );
				}
			}

			$args = array(
				'post_type' => esc_attr( $attributes['cptui_posttype'] ),
			);

			if ( isset( $attributes['taxonomy'] ) ) {
				$attributes['tax_query']['relation'] = apply_filters( 'cptui_tax_query_relation', 'AND' );
				foreach ( $attributes['taxonomy'] as $taxonomy => $value ) {

					$tax = array(
						'taxonomy' => $taxonomy,
						'field' => 'slug',
						'terms' => $value,
					);

					$attributes['tax_query'][] = $tax;
				}
			}

			/**
			 * Filter shortcode tempalte location
			 *
			 * @since 1.0.0
			 * @param array $attributes shortcode attributes.
			 */
			$template = apply_filters( 'cptui_shortcode_template',  cptui_locate_template( $attributes['cptui_shortcode'] ), $attributes );

			/**
			 * Hook when a shortcode loads
			 *
			 * @since 1.0.0
			 * @param array $attributes shortcode attributes.
			 */
			do_action( $attributes['cptui_shortcode'] . '_shortcode_loaded', $attributes );

			// Load scripts per shortcode.
			if ( isset( $attributes['cptui_shortcode'] ) && ! is_admin() ) {
				if ( $styles = cptui_get_shortcode_style( $attributes['cptui_shortcode'] ) ) {

					if ( is_array( $styles ) ) {
						foreach ( $styles as $style ) {
							wp_enqueue_style( $style );
						}
					} else {
						wp_enqueue_style( $styles );
					}
				}
				if ( $scripts = cptui_get_shortcode_script( $attributes['cptui_shortcode'] ) ) {

					if ( is_array( $scripts ) ) {
						foreach ( $scripts as $script ) {
							wp_enqueue_script( $script );
						}
					} else {
						wp_enqueue_script( $scripts );
					}
				}
			}

			$cptui_class = cptui_shortcode_classes( $attributes['cptui_shortcode'] );

			ob_start();
			echo '<div id="cptui" class="'. esc_attr( $cptui_class ) .'">';
			$shortcode = require( $template );
			echo '</div>';
			$shortcode = ob_get_contents();
			ob_end_clean();
			return $shortcode;

		}

		/**
		 * Override for attribute getter
		 *
		 * You can use this to override specific attribute acquisition
		 * ex. Getting attributes from options, post_meta, etc...
		 *
		 * @see WDS_Shortcode::att
		 *
		 * @param string      $att     Attribute to override.
		 * @param string|null $default Default value.
		 * @return string
		 */
		public function att( $att, $default = null ) {
			$current_value = parent::att( $att, $default );
			return $current_value;
		}
	}

}
