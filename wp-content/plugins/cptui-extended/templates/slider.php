<?php
/**
 * Bullet shortcode template
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @license GPLV2
 * @since 1.0.0
 */

/*
 * This file will have an $attributes array variable available to render various parts of the template. The values in
 * the array will be composed of attributes passed in to the shortcode.
 *
 * You can override it by placing a matching named file in ____.
 *
 * $args // Arguments to be used with WP_Query. Default: array( 'post_type' => 'post' )
 * $attributes // All shortcode attributes from post editor
 */

?>

	<?php
		$attributes = cptui_shortcode_atts( $attributes );
		$attributes['height'] = ( isset( $attributes['height'] ) && is_numeric( $attributes['height'] ) ) ? $attributes['height'] : '300';
	?>

	<?php
	/**
	 * Fires before the shortcode.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_list_before_shortcode', $attributes ); ?>

	<?php
		$custom_query = new WP_Query( cptui_filter_query( $attributes ) );
	?>

	<div class="cptui-shortcode-slider" data-slick='<?php echo cptui_slider_config( $attributes ); ?>'>

		<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

			<div class="post-<?php the_ID(); ?>">
				<a href="<?php the_permalink(); ?>"><div class="slider-image" style="background: url(<?php the_post_thumbnail_url( 'full' ); ?>) grey; background-size:cover; height:<?php echo esc_attr( $attributes['height'] ); ?>px"></div></a>

				<?php if ( 'on' === $attributes['show_title'] ) : ?>
					<a href="<?php the_permalink(); ?>" class="h5"><?php the_title(); ?></a>
				<?php endif ; ?>

			</div><!-- .post-xxx -->

		<?php endwhile; ?>

	</div><!-- .cptui-shortcode-list -->

	<?php
	/**
	 * Fires after the shortcode.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_list_after_shortcode', $attributes ); ?>

	<?php wp_reset_postdata(); // Reset the query. ?>
