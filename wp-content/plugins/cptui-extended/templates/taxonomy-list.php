<?php
/**
 * Taxonomy List shortcode template
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @license GPLV2
 * @since 1.0.0
 */

/*
 * This file will have an $attributes array variable available to render various parts of the template. The values in
 * the array will be composed of attributes passed in to the shortcode.
 *
 * You can override it by placing a matching named file in ____.
 *
 * $args // Arguments to be used with WP_Query. Default: array( 'post_type' => 'post' )
 * $attributes // All shortcode attributes from post editor
 */

?>

	<?php $attributes = cptui_shortcode_atts( $attributes ); ?>

	<?php
	/**
	 * Fires before the shortcode.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_taxonomy_list_before_shortcode', $attributes ); ?>

	<?php
		$taxonomies = isset( $attributes['taxonomy'] ) ? $attributes['taxonomy'] : [];
		$list_type = isset( $attributes['list_type'] ) && '' !== $attributes['list_type'] ? esc_attr( $attributes['list_type'] ) : 'ul';
	?>

		<?php  foreach ( $taxonomies as $taxonomy => $value ) : ?>

			<?php
			$taxon = get_taxonomy( $taxonomy );
			$taxon_label = isset( $taxon ) ? $taxon->labels->name : '';
			?>

			<h3 class="h4"><?php echo esc_attr( $taxon->labels->name ); ?></h3>

			<<?php echo $list_type; ?> class="cptui-shortcode-list">

				<?php  foreach ( $value as $data ) : ?>

					<?php
					/**
					 * Fires before the item.
					 *
					 * @param array $attributes shortcode atrributes.
					 * @since 1.1.0
					 */
					 do_action( 'template_taxonomy_list_before_item', $attributes ); ?>

					<?php
					 	$term_link = get_term_link( $data, $taxonomy );
						if ( is_wp_error( $term_link ) ) {
							$term_link = '';
						}
					?>

					<li class="post">
						<a href="<?php echo esc_url( $term_link ); ?>" class="h5"><?php echo esc_attr( $data ); ?></a>
					</li><!-- .post-xxx -->

				<?php endforeach; ?>

				<?php
				/**
				 * Fires after the item.
				 *
				 * @param array $attributes shortcode atrributes.
				 * @since 1.1.0
				 */
				 do_action( 'template_taxonomy_list_after_item', $attributes ); ?>

			 </<?php echo $list_type; ?>><!-- .cptui-shortcode-list -->

		<?php endforeach; ?>



	<?php
	/**
	 * Fires after the shortcode.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_taxonomy_list_after_shortcode', $attributes ); ?>
