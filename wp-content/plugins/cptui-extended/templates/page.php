<?php
/**
 * Page shortcode template
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @license GPLV2
 * @since 1.0.0
 */

/*
 * This file will have an $attributes array variable available to render various parts of the template. The values in
 * the array will be composed of attributes passed in to the shortcode.
 *
 * You can override it by placing a matching named file in ____.
 *
 * $attributes // All shortcode attributes from post editor
 */

?>
	<?php $attributes = cptui_shortcode_atts( $attributes ); ?>

	<?php
	/**
	 * Fires before the title.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_page_before_title', $attributes ); ?>

	<?php if ( isset( $attributes['title'] ) && '' !== $attributes['title'] ) : ?>
		<h3 class="h4"><?php cptui_shortcode_title( $attributes['title'] ); ?></h3>
	<?php endif; ?>

	<?php
	/**
	 * Fires after the title.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_page_after_title', $attributes ); ?>

	<?php $custom_query = new WP_Query( cptui_filter_query( $attributes ) ); ?>

	<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

		<?php
		/**
		 * Fires before the item.
		 *
		 * @param array $attributes shortcode atrributes.
		 * @since 1.1.0
		 */
		 do_action( 'template_page_before_item', $attributes ); ?>

		<article id="post-<?php the_ID(); ?>" <?php cptui_post_class( 'cptui-entry', $attributes ); ?>>



			<?php if ( 'on' === $attributes['featured_image'] ) : ?>
				<figure class="entry-thumbnail">

					<?php
					/**
					 * Fires before the featured image.
					 *
					 * @param array $attributes shortcode atrributes.
					 * @since 1.1.0
					 */
					do_action( 'template_page_before_featured_image', $attributes ); ?>

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail' ); ?></a>

					<?php
					/**
					 * Fires after the featured image.
					 *
					 * @param array $attributes shortcode atrributes.
					 * @since 1.1.0
					 */
					do_action( 'template_page_after_featured_image', $attributes ); ?>

				</figure>
			<?php endif; ?>

			<header class="entry-header">

				<?php
				/**
				 * Fires before the item title.
				 *
				 * @param array $attributes shortcode atrributes.
				 * @since 1.1.0
				 */
				do_action( 'template_page_before_item_title', $attributes ); ?>

				<h4 class="entry-title h5"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

				<?php
				/**
				 * Fires after the item title.
				 *
				 * @param array $attributes shortcode atrributes.
				 * @since 1.1.0
				 */
				do_action( 'template_page_after_item_title', $attributes ); ?>

			</header><!-- .entry-header -->

			<div class="entry-summary">

				<?php
				/**
				 * Fires before the excerpt.
				 *
				 * @param array $attributes shortcode atrributes.
				 * @since 1.1.0
				 */
				do_action( 'template_page_before_excerpt', $attributes ); ?>

				<?php the_excerpt(); ?>

				<?php
				/**
				 * Fires after the excerpt.
				 *
				 * @param array $attributes shortcode atrributes.
				 * @since 1.1.0
				 */
				do_action( 'template_page_after_excerpt', $attributes ); ?>

			</div><!-- .entry-summary -->

		</article><!-- .cptui-entry -->

		<?php
		/**
		 * Fires after the item.
		 *
		 * @param array $attributes shortcode atrributes.
		 * @since 1.1.0
		 */
		do_action( 'template_page_after_item', $attributes ); ?>

	<?php endwhile; ?>
	<?php wp_reset_postdata(); // Reset the query. ?>
