<?php
/**
 * Easy Digital Downloads shortcode template
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @license GPLV2
 * @since 1.0.0
 */

/*
 * This file will have an $attributes array variable available to render various parts of the template. The values in
 * the array will be composed of attributes passed in to the shortcode.
 *
 * You can override it by placing a matching named file in ____.
 *
 * $args // Arguments to be used with WP_Query. Default: array( 'post_type' => 'post' )
 * $attributes // All shortcode attributes from post editor
 */

?>

	<?php $attributes = cptui_shortcode_atts( $attributes ); ?>

	<?php
	/**
	 * Fires before the title.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_edd_before_title', $attributes ); ?>

	<?php if ( isset( $attributes['title'] ) && '' !== $attributes['title'] ) : ?>
		<h3 class="h4"><?php cptui_shortcode_title( $attributes['title'] ); ?></h3>
	<?php endif; ?>

	<?php $custom_query = new WP_Query( cptui_filter_query( $attributes ) ); ?>

	<div class="cptui-shortcode-list cptui-shortcode-edd-list product-list">

		<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

			<?php
			/**
			 * Fires before the item.
			 *
			 * @param array $attributes shortcode atrributes.
			 * @since 1.1.0
			 */
			 do_action( 'template_edd_before_item', $attributes ); ?>

			<span itemscope itemtype="http://schema.org/Product">
				<article id="post-<?php the_ID(); ?>" <?php post_class( 'cptui-entry' ); ?>>

					<?php if ( 'on' === $attributes['featured_image'] ) : ?>

						<figure class="entry-thumbnail">

							<?php
							/**
							 * Fires before the featured image.
							 *
							 * @param array $attributes shortcode atrributes.
							 * @since 1.1.0
							 */
							do_action( 'template_edd_before_featured_image', $attributes ); ?>

							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail' ); ?></a>

							<?php
							/**
							 * Fires after the featured image.
							 *
							 * @param array $attributes shortcode atrributes.
							 * @since 1.1.0
							 */
							do_action( 'template_edd_after_featured_image', $attributes ); ?>

						</figure>

					<?php endif; ?>

					<header class="entry-header">

						<?php
						/**
						 * Fires before the item title.
						 *
						 * @param array $attributes shortcode atrributes.
						 * @since 1.1.0
						 */
						do_action( 'template_edd_before_item_title', $attributes ); ?>

						<h4 class="entry-title h5">
							<span itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
						</h4>

						<?php
						/**
						 * Fires after the item title.
						 *
						 * @param array $attributes shortcode atrributes.
						 * @since 1.1.0
						 */
						do_action( 'template_edd_after_item_title', $attributes ); ?>

					</header><!-- .entry-header -->

					<div class="entry-summary" itemprop="description">

						<?php
						/**
						 * Fires before the excerpt.
						 *
						 * @param array $attributes shortcode atrributes.
						 * @since 1.1.0
						 */
						do_action( 'template_edd_before_excerpt', $attributes ); ?>

						<?php the_excerpt(); ?>

						<?php
						/**
						 * Fires after the excerpt.
						 *
						 * @param array $attributes shortcode atrributes.
						 * @since 1.1.0
						 */
						do_action( 'template_edd_after_excerpt', $attributes ); ?>

					</div><!-- .entry-summary -->

					<footer class="entry-footer">
						<?php $product = edd_get_download( get_the_ID() ); ?>

						<div class="product-price">

							<?php
							/**
							 * Fires before the price.
							 *
							 * @param array $attributes shortcode atrributes.
							 * @since 1.1.0
							 */
							do_action( 'template_edd_before_price', $attributes ); ?>

							<?php if ( ! edd_has_variable_prices( get_the_ID() ) ) : ?>
								<?php esc_attr_e( 'Price: ', 'cptuiext' ); ?>
								<?php edd_price( get_the_ID() ); ?>
							<?php endif; ?>

							<?php
							/**
							 * Fires after the price.
							 *
							 * @param array $attributes shortcode atrributes.
							 * @since 1.1.0
							 */
							do_action( 'template_edd_after_price', $attributes ); ?>

						</div>

						<div class="product-checkout">

							<?php
							/**
							 * Fires before the checkout.
							 *
							 * @param array $attributes shortcode atrributes.
							 * @since 1.1.0
							 */
							do_action( 'template_edd_before_checkout', $attributes ); ?>

							<?php echo edd_get_purchase_link( array( 'download_id' => get_the_ID() ) ); ?>

							<?php
							/**
							 * Fires after the checkout.
							 *
							 * @param array $attributes shortcode atrributes.
							 * @since 1.1.0
							 */
							do_action( 'template_edd_after_checkout', $attributes ); ?>

						</div>
					</footer><!-- .entry-footer -->

				</article><!-- .cptui-entry -->
			</span>

		<?php endwhile; ?>

		<?php
		/**
		 * Fires before pagination.
		 *
		 * @since 1.1.0
		 */
		do_action( 'template_edd_before_pagination', $attributes ); ?>

		<?php cptui_pagination_links( $custom_query, $attributes ); ?>

		<?php
		/**
		 * Fires after the item.
		 *
		 * @since 1.1.0
		 */
		do_action( 'template_edd_after_item', $attributes ); ?>

	</div><!-- .cptui-shortcode-list .cptui-shortcode-edd-list .product-list .unlist -->

	<?php wp_reset_postdata(); // Reset the query. ?>
