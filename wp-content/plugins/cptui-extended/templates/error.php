<?php
/**
 * Shortcode error template
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @license GPLV2
 * @since 1.0.0
 */

/*
 * This file will display if a shortcode template file is not found or an error with the shortcode data.
 *
 */
echo '<span class="error">' . sprintf( __( 'Sorry, error displaying %s shortcode.', 'cptuiext' ), esc_attr( $attributes['cptui_shortcode'] ) ) . '</span>';
