<?php
/**
 * Bullet shortcode template
 *
 * @package CPTUIExtended
 * @author WebDevStudios
 * @license GPLV2
 * @since 1.0.0
 */

/*
 * This file will have an $attributes array variable available to render various parts of the template. The values in
 * the array will be composed of attributes passed in to the shortcode.
 *
 * You can override it by placing a matching named file in ____.
 *
 * $args // Arguments to be used with WP_Query. Default: array( 'post_type' => 'post' )
 * $attributes // All shortcode attributes from post editor
 */

?>

	<?php $attributes = cptui_shortcode_atts( $attributes ); ?>

	<?php
	/**
	 * Fires before the shortcode.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_list_before_shortcode', $attributes ); ?>

	<?php
		$custom_query = new WP_Query( cptui_filter_query( $attributes ) );
		$list_type = isset( $attributes['list_type'] ) ? esc_attr( $attributes['list_type'] ) : 'ul';
	?>

	<<?php echo esc_attr( $list_type ); ?> class="cptui-shortcode-list">

		<?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>

			<?php
			/**
			 * Fires before the item.
			 *
			 * @param array $attributes shortcode atrributes.
			 * @since 1.1.0
			 */
			 do_action( 'template_list_before_item', $attributes ); ?>

			<li class="post-<?php the_ID(); ?>">
				<a href="<?php the_permalink(); ?>" class="h5"><?php the_title(); ?></a>

				<?php if ( isset( $attributes['excerpt'] ) && 'on' === $attributes['excerpt'] ) : ?>
					<div class="entry-summary">

						<?php
						/**
						 * Fires before the excerpt.
						 *
						 * @param array $attributes shortcode atrributes.
						 * @since 1.1.0
						 */
						do_action( 'template_list_before_excerpt', $attributes ); ?>

						<?php the_excerpt(); ?>

						<?php
						/**
						 * Fires before the excerpt.
						 *
						 * @param array $attributes shortcode atrributes.
						 * @since 1.1.0
						 */
						do_action( 'template_list_after_excerpt', $attributes ); ?>

					</div>
				<?php endif; ?>
			</li><!-- .post-xxx -->

		<?php endwhile; ?>

		<?php
		/**
		 * Fires after the item.
		 *
		 * @param array $attributes shortcode atrributes.
		 * @since 1.1.0
		 */
		do_action( 'template_list_after_item', $attributes ); ?>

	</<?php echo $list_type; ?>><!-- .cptui-shortcode-list -->

	<?php
	/**
	 * Fires after the shortcode.
	 *
	 * @param array $attributes shortcode atrributes.
	 * @since 1.1.0
	 */
	do_action( 'template_list_after_shortcode', $attributes ); ?>

	<?php wp_reset_postdata(); // Reset the query. ?>
