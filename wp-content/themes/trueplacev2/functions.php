<?php
/**
 * TruePlaceV2 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package TruePlaceV2
 */

if ( ! function_exists( 'trueplacev2_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function trueplacev2_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on TruePlaceV2, use a find and replace
	 * to change 'trueplacev2' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'trueplacev2', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'trueplacev2' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'trueplacev2_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'trueplacev2_setup' );



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function trueplacev2_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'trueplacev2_content_width', 640 );
}
add_action( 'after_setup_theme', 'trueplacev2_content_width', 0 );



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function trueplacev2_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'trueplacev2' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'trueplacev2' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'trueplacev2_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function trueplacev2_scripts() {
	wp_enqueue_style( 'trueplacev2-style', get_stylesheet_uri() );

	wp_enqueue_script( 'trueplacev2-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'trueplacev2-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'trueplacev2_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

add_action( 'init', 'cptui_register_my_cpts_clients' );
function cptui_register_my_cpts_clients() {
    $labels = array(
        "name" => __( 'Clients', 'True Place v2' ),
        "singular_name" => __( 'Client', 'True Place v2' ),
        "menu_name" => __( 'Clients', 'True Place v2' ),
        "all_items" => __( 'All Clients', 'True Place v2' ),
        "add_new" => __( 'Add New', 'True Place v2' ),
        "add_new_item" => __( 'Add New Client', 'True Place v2' ),
        "edit_item" => __( 'Edit Client', 'True Place v2' ),
        "new_item" => __( 'New Client', 'True Place v2' ),
        "view_item" => __( 'View Client', 'True Place v2' ),
        "search_items" => __( 'Search Client', 'True Place v2' ),
        "not_found" => __( 'No Client Found', 'True Place v2' ),
        "not_found_in_trash" => __( 'No Client Found in Trash', 'True Place v2' ),
        "parent" => __( 'Parent Client', 'True Place v2' ),
        "featured_image" => __( 'Featured Image', 'True Place v2' ),
        "set_featured_image" => __( 'Set Featured Image', 'True Place v2' ),
        "remove_featured_image" => __( 'Remove Featured Image', 'True Place v2' ),
        "use_featured_image" => __( 'Use Featured Image', 'True Place v2' ),
        "archives" => __( 'Client Archive', 'True Place v2' ),
        "insert_into_item" => __( 'Insert into Client', 'True Place v2' ),
        "uploaded_to_this_item" => __( 'Uploaded to this Client', 'True Place v2' ),
        "filter_items_list" => __( 'Filter Client List', 'True Place v2' ),
        "items_list_navigation" => __( 'Client List Navigation', 'True Place v2' ),
        "items_list" => __( 'Client List', 'True Place v2' ),
    );

    $args = array(
        "label" => __( 'Clients', 'True Place v2' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "clients", "with_front" => true ),
        "query_var" => true,
        "menu_position" => 6,
        "supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
    );
    register_post_type( "clients", $args );

// End of cptui_register_my_cpts_clients()
}

function my_cptui_add_post_types_to_archives( $query ) {
    // We do not want unintended consequences.
    if ( is_admin() || ! $query->is_main_query() ) {
        return;
    }

    if ( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
        $cptui_post_types = cptui_get_post_type_slugs();

        $query->set(
            'post_type',
            array_merge(
                array( 'post' ),
                $cptui_post_types
            )
        );
    }
}
add_filter( 'pre_get_posts', 'my_cptui_add_post_types_to_archives' );

