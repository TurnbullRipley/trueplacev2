<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
</div>
</div><!-- .site-content -->
<footer class="row">
	<div class="container footer">
		<div class="col l6 m6 s12">
			<div class="row">
				<ul class="social">
					<li>Connect with us</li>
					<li><a href="https://twitter.com/TruePlace1"><img src="/assets/2016/img/icon-twitter.jpg" width="28" height="28" alt=""/></a></li>
					<li><a href="https://www.linkedin.com/company/true-place-consulting"><img src="/assets/2016/img/icon-linkedin.jpg" width="28" height="28" alt=""/></a></li>
					<li><a href="#"><img src="/assets/2016/img/icon-rss.jpg" width="28" height="28" alt=""/></a></li>
				</ul>
			</div>

		</div>
		<div class="col l6 m6 s12">
			<div class="row">
				<ul class="links">
					<li><a href="#">Contact us</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li>&copy; True Place Conculting 2016</li>
				</ul>
			</div>

		</div>
	</div>
</footer><!-- .site-footer -->
<script src="/assets/vendor/jquery-3.1.0/jquery-3.1.0.js"></script>
<script src="/assets/2016/js/jquery.functions.js"></script>
<?php wp_footer(); ?>
</body>
</html>
