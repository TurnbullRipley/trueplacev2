<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package TruePlaceV2
 */

get_header(); ?>
<!-- Single.php -->
	<div class="row">
		<div class="col s12">
			<?php
			the_title( '<h2>', '</h2>' );
			?>
		</div>
	</div>
	<div class="row">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content-left', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile;
		?>
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content-right', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile;
		?>

	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
