<?php /* Template Name: Team */ ?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TruePlaceV2
 */

get_header(); ?>
	<!-- Page-team.php -->
	<div class="row">
		<div class="col l3 m5 s12 outset-l9 outset-m7">
            <h2>Who We Are</h2>
		</div>
	</div>
	<div class="row">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile;
			?>

	</div><!-- #primary -->

<?php
get_footer();
