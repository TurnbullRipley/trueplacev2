<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TruePlaceV2
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<!-- Favicons -->
	<link rel="shortcut icon" href="/assets/2016/img/favicon.png">
	<link rel="apple-touch-icon" href="/assets/2016/img/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/2016/img/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/2016/img/apple-touch-icon-114x114.png">
	<!-- CSS  -->
	<link href="http://fonts.googleapis.com/css?family=Copse" rel="stylesheet" type="text/css">
    <link href="/assets/vendor/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="/assets/core/css/normalize.css">
	<link rel="stylesheet" href="/assets/core/css/skeleton.css">
	<link rel="stylesheet" href="/assets/core/css/menu.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="row header">
	<div class="container">
		<div class="row">
			<div id="logo" class="col l4 m4 s12">
                <div class="row">
                    <a href="/"><img src="/assets/2016/img/logo-true-place.png" alt="True Place Consulting" /></a>
                </div>

			</div>
			<div class="col l8 m8 s12">
                <i class="fa fa-2x fa-navicon menu"></i>
				<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'trueplacev2' ); ?>">
						<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu',
						) );
						?>
					</nav><!-- .main-navigation -->
				<?php endif; ?>
			</div>
		</div>
	</div>
</div><!-- .site-header -->
<div class="row">
	<div id="main" class="container">
