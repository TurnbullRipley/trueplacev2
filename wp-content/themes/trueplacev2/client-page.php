<?php /* Template Name: Client */ ?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TruePlaceV2
 */

get_header(); ?>
	<!-- Page-clients.php -->
	<div class="row">
		<div class="col l3 m5 s12 outset-l9 outset-m7">
            <h2>Who We Do It For</h2>
		</div>
	</div>
	<div class="row">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/clients', 'page' );


			endwhile;
			?>

	</div><!-- #primary -->

<?php
get_footer();
