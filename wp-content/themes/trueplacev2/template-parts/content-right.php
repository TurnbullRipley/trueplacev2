<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TruePlaceV2
 */

?>
<div class="col l6 m6 s12">
    <div class="row">
        <?php the_field('right_content'); ?>
    </div>
</div>



