$(document).ready(function(){
    $('ul.accordion h4').click(function(){
        var checkElement = $(this).next('div');
        if(!checkElement.is(':visible')) {
            $('ul.accordion li div').slideUp(600);
            $(checkElement).slideDown(600);
        }else{
            $(checkElement).slideUp(600);
        }
    });
    $('.menu').click(function(){
        $('nav ul:first-child').slideToggle(500);

    });
});